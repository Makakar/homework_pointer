#include <iostream>
using namespace std;

// C����� ����
#define STACK_SIZE 100

class Stack
{
private:
	int arrayStack[STACK_SIZE];
	int head = -1;

public:
	// �������� ������� ���� � ��� ���� �� ������������
	void push(const int x)
	{   
		// �� ����� �������� ����
		head++;
		// � = head
		arrayStack[head] = x;
	}

	// ������� ������� ����
	int pop()
	{
		// ������ ������ �������, ������� ����� ���������� 
		int temp = arrayStack[head];
		// �� ����� ������� �� ������ �� �����
		head--;
		// ���������
		return temp;
	}

	int getStackSize()
	{	
		// �� ����, ��� ��������...
		return head + 1;
	}
};

int main()
{   // ������ ������ ������ Stack
	Stack st;

	// ������� � ���� ��������� ���������
	st.push(3);
	st.push(-700);
	st.push(1024);
	st.push(65536);
	st.push(-7989489);

	// �������� 
	cout << "V steke " << st.getStackSize() << " elementov:" << endl;
	cout << "--------------------------------" << endl;

	while (st.getStackSize() > 0)
	{
		cout << st.pop() << endl;
	}

	cout << "--------------------------------" << endl;

}
